import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild, } from '@angular/core';

import { ChatService } from '../chat.service';
import { Subject, interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, AfterViewChecked {
  @ViewChild('chat') private myScrollContainer: ElementRef;
  notifyChatRoom = new Subject();
  rooms: any;
  newMessage: String;
  messages: any;
  activeRoom: Number = null;
  activeRoomData: any;
  timer: any;
  constructor(private chatService: ChatService) {
    this.notify();
  }

  ngOnInit() {
    this.chatService.getRooms().subscribe(roomsList => {
      this.rooms = roomsList;
    });
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  private notify() {
    this.notifyChatRoom.next(this.messages);
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  getCurrentUser() {
    return localStorage.getItem('chatuser');
  }

  openChatRoom(roomNumber) {
    this.chatService.getRoomDetails(roomNumber).subscribe(roomData => {
      this.activeRoomData = roomData;
    });
    this.activeRoom = roomNumber;
    this.chatService.getMessages(roomNumber).subscribe(messages => {
      this.messages = messages;
      this.notify();
      this.scrollToBottom();
    });
  }

  sendMessage() {
    const messageData = {
      name: localStorage.getItem('chatuser'),
      message: this.newMessage
    };
    this.chatService.sendMessage(this.activeRoom, messageData).subscribe(message => {
      this.messages.push(message);
      if (this.activeRoomData.users.indexOf(localStorage.getItem('chatuser')) === -1) {
        this.activeRoomData.users.push(localStorage.getItem('chatuser'));
      }
      this.newMessage = '';
      this.notify();
      this.scrollToBottom();
    });
  }

}
