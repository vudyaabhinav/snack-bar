import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName: string;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  login() {
    if (!!this.userName) {
      localStorage.setItem('chatuser', this.userName);
      this.router.navigateByUrl('/chat');
    } else {
      alert('Please enter a user name');
    }
  }
}
