# DoorDash Frontend Interview Project

## Howdy! 👋  

This is the frontend interview project for DoorDash! The technologies used in this project are Bootstrap 4, Angular 6, Docker, Angular-cli

## Summary:

### Reason for Angular usage:
  Intense familiarity resulting faster completion(given the timeframe of 3 hours) and performance difference between Angular and React apart from angular coming with additional plugins.

### Reason for Docker usage:
  Containerization makes it easier to build, test and share application rather than installing multiple instances and ensuring consistency.

## Getting started
There's a few things you need to get started on to get this to work.

### 1. Installing minimum deps
Install Docker

### 2. Serving your app
Change Directory into the cloned application and run `docker-compose up` and access the application on `localhost:4200`

