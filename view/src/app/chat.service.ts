import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: Http) { }

  getRooms() {
    return this.http.get('http://localhost:3000/api/rooms').pipe(map(res => res.json()));
  }

  getRoomDetails(roomId) {
    return this.http.get(`http://localhost:3000/api/rooms/${roomId}`).pipe(map(res => res.json()));
  }

  getMessages(roomId) {
    return this.http.get(`http://localhost:3000/api/rooms/${roomId}/messages`).pipe(map(res => res.json()));
  }

  sendMessage(roomId, message) {
    return this.http.post(`http://localhost:3000/api/rooms/${roomId}/messages`, message).pipe(map(res => res.json()));
  }
}
